package com.softserve.edu.task3;

import java.util.*;

public class App {
	public static void main(String[] args) {
		final String INSTRUSTION = "HOW TO USE\n" + "This programm is for descending sorting of triangles.\n"
				+ "First of all we need to enter triangles in console in a specific format: \n"
				+ "name,side1,side2,side3 Example: triangle1,1.0,1.0,1.0\". Programm asks weather \n"
				+ "we want add another one triangle. If Y or Yes answer than we input it as it was before\n"
				+ "After pressing any key, accept Y/Yes after question \"Do you want add another one?\" \n"
				+ "the program output the triangles into descending order.";
		int exitsign = 1;
		List<Triangle> setoftriangles = new ArrayList<Triangle>();
		System.out.println(INSTRUSTION);
		System.out.println();
		do {
			exitsign = 1;
			String str = "";
			System.out.println("Input triangle as : name,side1,side2,side3");
			Scanner sc = new Scanner(System.in);
			str = sc.nextLine().toLowerCase();
			StringTokenizer strtoparce = new StringTokenizer(str, ",");
			List<String> setofstr = new ArrayList<String>();
			while (strtoparce.hasMoreTokens()) {
				setofstr.add(strtoparce.nextToken());
			}
			if (setofstr.size() == 4) {
				String name = setofstr.get(0).trim();
				double side1 = 0;
				double side2 = 0;
				double side3 = 0;

				try {
					side1 = Double.parseDouble(setofstr.get(1).trim());
					side2 = Double.parseDouble(setofstr.get(2).trim());
					side3 = Double.parseDouble(setofstr.get(3).trim());
				} catch (NullPointerException e) {
					System.out.println("Error! Input value must not be a null.");
					exitsign = 2;
				} catch (NumberFormatException e) {
					System.out.println("Error! Input value must must be double.");
					exitsign = 2;
				}

				Triangle triangle = new Triangle();
				if (triangle.isTriangle(side1, side2, side3)) {
					triangle.setName(name);
					triangle.setSide1(side1);
					triangle.setSide2(side2);
					triangle.setSide3(side3);
					setoftriangles.add(triangle);
				} else {
					System.out.println("Error! Inputed sides are not triangle's sides");
					exitsign = 2;
				}

				System.out.println("Do you want add another one? ( press \"y\" / \"yes\")");
				String yesnochoice;
				yesnochoice = sc.nextLine().toLowerCase();
				if (yesnochoice.equals("yes") || yesnochoice.equals("y")) {
					exitsign = 2;
				} else {

					Collections.sort(setoftriangles);
					sc.close();
					for (int i = 0; i < setoftriangles.size(); i++) {
						System.out.println(
								"[" + setoftriangles.get(i).getName() + "]" + ":" + setoftriangles.get(i).square());
					}
				}
			} else {
				System.out.println("Error! Check input format. It must be: name,side1,side2,side3");
				exitsign = 2;
			}
		} while (exitsign != 1);
	}
}
