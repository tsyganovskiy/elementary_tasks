package com.softserve.edu.task3;

public class Triangle implements Comparable<Triangle>{
	private String name;
	private double side1;
	private double side2;
	private double side3;

	public void setSide1(double side1) {
		this.side1 = side1;
	}

	public void setSide2(double side2) {
		this.side2 = side2;
	}

	public void setSide3(double side3) {
		this.side3 = side3;
	}

	public void setName(String name) {
		this.name = name;
	}
	
	public String getName(){
		return name;
	}

	public boolean isTriangle(double side1, double side2, double side3) {
		if (((side1 + side2) > side3) && ((side1 + side3) > side2) && (side2 + side3) > side1) {
			return true;
		} else {
			return false;
		}
	}

	public double square() {
		double sqr = 0;
		sqr = Math.sqrt(0.5 * (side1 + side2 + side3) * ((0.5 * (side1 + side2 + side3) - side1))
				* ((0.5 * (side1 + side2 + side3)) - side2) * ((0.5 * (side1 + side2 + side3)) - side3));
		return sqr;
	}

	@Override
	public int compareTo(Triangle obj) {
		 return Double.compare(obj.square(),this.square());
	}
}
