package com.softserve.edu.task4;

import java.io.*;
import java.util.*;

public class App {
	public static void main(String[] args) throws Exception {
		final String INSTRUCTION = "HOW TO USE\n"
				+ "This programm has two way to run.\nThe first one is when 2 input parameters:\n"
				+ "1.Filepath \n2.A \"string\" which occurance is needed to count.\n\n"
				+ "The second way is when 3 input parameters:\n"
				+ "1.Filepath \n2.A \"string1\" which occurance is needed to count and replace\n"
				+ "3.A \"string2\" which will be insert instead of \"string2\"";
		if (args == null || args.length != 2 || args.length != 3) {
			System.out.println("Error! Must be 2 or 3 input values.The programm will be closed");
			System.out.println();
			System.out.println(INSTRUCTION);
			System.exit(0);
		}

		if (args.length == 2) {
			try {
				BufferedReader in = new BufferedReader(new FileReader(args[0]));
				Scanner scanner = new Scanner(in);
				int result = 0;
				String basestring;
				String stringtofind;

				stringtofind = args[1].toLowerCase();

				while (scanner.hasNextLine()) {
					basestring = scanner.nextLine().toLowerCase();
					for (int i = 0; i <= (basestring.length() - stringtofind.length()); i++) {
						if (basestring.substring(i, i + stringtofind.length()).equalsIgnoreCase(stringtofind)) {
							result++;
						}
					}
				}
				scanner.close();
				in.close();

				System.out.println("The string " + "\"" + stringtofind + "\"" + " is met  " + result + " times");

			} catch (IOException e) {
				System.out.println("Error! Cann't find the file.The programm will be closed");
				System.exit(0);
			}
		}

		if (args.length == 3) {
			try {
				BufferedReader in = new BufferedReader(new FileReader(args[0]));

				Scanner scanner = new Scanner(in);
				String stringtofind;
				String stringtoreplace;
				StringBuffer basestring; ;
				StringBuffer fulltextinstring = new StringBuffer();
				stringtofind = args[1].toLowerCase();
				stringtoreplace = args[2].toLowerCase();
				while (scanner.hasNextLine()) {
					basestring = new StringBuffer(scanner.nextLine().toLowerCase());
					for (int i = 0; i <= (basestring.length() - stringtofind.length()); i++) {
						if (basestring.substring(i, i + stringtofind.length()).equalsIgnoreCase(stringtofind)) {
							basestring.insert(i, stringtoreplace);
							basestring.delete(i + stringtoreplace.length(),
									i + stringtoreplace.length() + stringtofind.length());
						}
					}
					fulltextinstring.append(basestring).append('\n');
				}
				try {
					FileWriter fw = new FileWriter(args[0]);
					BufferedWriter bw = new BufferedWriter(fw);
					bw.write(fulltextinstring.toString());
					bw.close();
					fw.close();
				} catch (IOException e) {
					System.out.println("Error! Cann't find the file.The programm will be closed");
					System.exit(0);
				}
				scanner.close();
				in.close();
			} catch (IOException e) {
				System.out.println("Error! Cann't find the file.The programm will be closed");
				System.exit(0);
			}
		}
	}
}
