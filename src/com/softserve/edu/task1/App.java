package com.softserve.edu.task1;

public class App {

	public static void main(String[] args) {
		final String INSTRUCTION = "HOW TO USE \n" + "This programm outputs in console "
				+ " an array of '*'-char like a chess board.\n" + "To start you should enter two positive integer "
				+ " numbers as an argument splited by \"space\" sign + HOW TO USE \n" +
				"\" + \"This programm outputs in console \"\n" +
				"\t\t\t\t+ \" an array of '*'-char like a chess board.\\n\" + \"To start you should enter two positive integer \"\n" +
				"\t\t\t\t+ \" numbers as an argument splited by \"space\" sign";

		if (args.length != 2) {
			System.out.println("Input error.Must be two input parameters");
			System.out.println();
			System.out.println(INSTRUCTION);
			System.exit(0);
		}
        int width = 0;
        int heigth = 0;
        
        if (new Validator().isInputPositiveInteger(args[0])){
        	width = Integer.parseInt(args[0]);
        } 
        if (new Validator().isInputPositiveInteger(args[1])){
        	heigth = Integer.parseInt(args[1]);
        } 
		ChessBoard chessboard = new ChessBoard();
		chessboard.setWidth(width);
		chessboard.setHeigth(heigth);
		char[][] res =  chessboard.show();
		for (int i = 0; i < heigth; i++) {
			for (int j = 0; j < width; j++) {
				System.out.print(res[i][j]);

			}
			System.out.println();
		}
	}
}
