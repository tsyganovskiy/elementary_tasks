package com.softserve.edu.task1;

public  class ChessBoard {

	private int width;
	private int heigth;
	
	public void setWidth (int width){
		this.width = width;
	}
	
	public void setHeigth (int heigth){
		this.heigth = heigth;
	}
	public int getWidth (){
		return width;
	}
	
	public int getHeigth (){
		return heigth;
	}

	char[][] starsArray = new char[heigth][width];

	public char[][] show() {
		char[][] starsArray = new char[heigth][width];
		for (int i = 0; i < heigth; i++) {
			for (int j = 0; j < width; j++) {
				starsArray[i][j] = '*';

			}
		}

		for (int i = 0; i < heigth; i++) {
			for (int j = 0; j < width; j++) {
				if (!(i % 2 == 0) && (j % 2 == 0)) {
					starsArray[i][j] = ' ';
				} else if ((i % 2 == 0) && (!(j % 2 == 0))) {
					starsArray[i][j] = ' ';
				}
			}
		}

		return starsArray;

	}
}
