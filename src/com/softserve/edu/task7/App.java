package com.softserve.edu.task7;

public class App {
	public static void main(String[] args) {
		final String INSTRUCTION = "HOW TO USE\n" + "This programm outputs all numbers whose 2 power less than input number.\n"
				+ "You should input any positive integer.";
		if (args.length != 1) {
			System.out.println("Input error.Must be only one input parameter.The program will be closed.\n");
			System.out.println(INSTRUCTION);
			System.exit(0);
		}

		int number = 0;
		try {
			number = Integer.parseInt(args[0]);
		} catch (NullPointerException e) {
			System.out.println("Error! Input value must not be a null.The program will be closed.\n");
			System.exit(0);
		} catch (NumberFormatException e) {
			System.out.println("Error! Input value must  be integer.The program will be closed.\n");
			System.exit(0);
		}
		if (number < 0) {
			System.out.println("Error! Input value must  be positive integer.The programe will be closed.\n");
			System.exit(0);
		}
		
		String res = "";
		for (int i = 0; i < number; i++) {
			if ((i * i) < number) {
				res += i + ",";
			}
		}
		System.out.print("Result is : " + res.substring(0, res.length() - 1));
	}

}
