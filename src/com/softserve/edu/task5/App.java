package com.softserve.edu.task5;

public class App {
	public static void main(String[] args) {
		final String INSTRUCTION = "HOW TO USE\n" + "This programm converts a number to its representation in words\n"
				+ "You should input a number in range 1..998998998";
		if (args.length != 1) {
			System.out.println("Input error.Must be only one input parameter.The program will be closed.\n");
			System.out.println(INSTRUCTION);
			System.exit(0);
		}
		int number = 0;
		try {
			number = Integer.parseInt(args[0]);
		} catch (NumberFormatException e) {
			System.out.println("Error! Input value must  be integer.The program will be closed.");
			System.exit(0);			
		}
		if (number > 0) {
			InWords iw = new InWords(number);
			System.out.println(iw.show());
		} else {
			System.out.println("Error! Input value must  be positive.The program will be closed.");
			System.exit(0);
		}
	}
}
