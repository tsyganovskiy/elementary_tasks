package com.softserve.edu.task8;

import java.util.ArrayList;
import java.util.List;

public class Fibonacci {
	int start;
	int finish;

	Fibonacci(int start, int finish) {
		this.start = start;
		this.finish = finish;
	}

	public String show() {
		List<Integer> fib = new ArrayList<Integer>();
		fib.add(1);
		fib.add(1);
		int count = 1;
		if (fib.get(1) < finish) {
			do {
				count++;
				fib.add((fib.get(count - 1) + fib.get(count - 2)));
			} while (fib.get(count) < finish);

			String res = "";
			for (int i = 0; i < fib.size(); i++) {
				if ((fib.get(i) <= finish) && (fib.get(i) >= start)) {
					res += fib.get(i) + ",";
				}
			}
			return  res.substring(0, res.length() - 1);
		} else {
			return  fib.get(0) + "," + fib.get(1);
		}
	}
}
