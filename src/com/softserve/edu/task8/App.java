package com.softserve.edu.task8;

public class App {

	public static void main(String[] args) {
		final String INSTRUCTION = "HOW TO USE\n"
				+ "This programm outputs all Fibonacci numbers from input range.\n"
				+ "You should input two positive integer splitted by \"space\".\n"
				+ "Boundary values are included into output";
		int begin = 0;
		int end = 0;
		
		if (args.length != 2) {
			System.out.println("Input error.Must be two input parameter.The program will be closed.\n");
			System.out.println(INSTRUCTION);
			System.exit(0);
		}

		try {
			begin = Integer.parseInt(args[0]);
			end = Integer.parseInt(args[1]);
		} catch (NullPointerException e) {
			System.out.println("Error! Input value must not be a null.The program will be closed.\n");
			System.out.println(INSTRUCTION);
			System.exit(0);
		} catch (NumberFormatException e) {
			System.out.println("Error! Input value must  be integer.The program will be closed.\n");
			System.out.println(INSTRUCTION);
			System.exit(0);
		}

		if (end < 0 || begin < 0) {
			System.out.println("Error! Input values must be positive. The program will be closed.\n");
			System.out.println(INSTRUCTION);
			System.exit(0);
		}

		Fibonacci fb = new Fibonacci(begin, end);
		System.out.println("Result is " + fb.show());
	}
}
