package com.softserve.edu.task2;

public class Envelope {
	private double width;
	private double heigth;

	public void setWidth(double width) {
		this.width = width;
	}

	public void setHeigth(double heigth) {
		this.heigth = heigth;
	}

	public double getWidth() {
		return width;
	}

	public double getHeigth() {
		return heigth;
	}

	public String putInside(Envelope obj) {
		if (((this.width >= obj.width) || (this.heigth >= obj.heigth))
				&& ((this.width >= obj.heigth) || (this.heigth >= obj.width))) {
			return "Envelope �1 cann't be put in �2";
		} else {
			return "Envelope �1 can be put in �2";
		}
	}
}
