package com.softserve.edu.task2;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class App {

	public static void main(String[] args) {
		final String INSTRUSTION = "HOW TO USE\n" + "This programm checks wether we can put one rectangle envelope\n"
				+ "into other. To do that you should  type width and heigth of envelope 1\n"
				+ "and do the same for envelope 2. After that press enter to get result.\n"
				+ "Choose y or yes to start the program again.";
		int exitpointer = 1;
		double width1 = 0;
		String inputWidth1;
		System.out.println(INSTRUSTION);
		do {
			do {
				try {
					System.out.println();
					System.out.println("Input width of envelope �1");
					BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
					inputWidth1 = br1.readLine();
					if (Double.parseDouble(inputWidth1.replace(',', '.')) > 0) {
						width1 = Double.parseDouble(inputWidth1.replace(',', '.'));
						exitpointer = 1;
					} else {
						System.out.println("Error! Input value must positive");
						exitpointer = 2;
					}

				} catch (NullPointerException e) {
					System.out.println("Error! Input value must not be a null.");
					exitpointer = 2;
				} catch (NumberFormatException e) {
					System.out.println("Error! Input value must must be double.");
					exitpointer = 2;
				} catch (IOException e) {
					System.out.println(e.getStackTrace());
				}
			} while (exitpointer != 1);

			double heigth1 = 0;
			String inputHeigth1;

			do {
				try {
					System.out.println("Input heigth of envelope �1");
					BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
					inputHeigth1 = br1.readLine();
					if (Double.parseDouble(inputHeigth1.replace(',', '.')) > 0) {
						heigth1 = Double.parseDouble(inputHeigth1.replace(',', '.'));
						exitpointer = 1;
					} else {
						System.out.println("Error! Input value must positive");
						exitpointer = 2;
					}
				} catch (NullPointerException e) {
					System.out.println("Error! Input value must not be a null.");
					exitpointer = 2;
				} catch (NumberFormatException e) {
					System.out.println("Error! Input value must must be double.");
					exitpointer = 2;
				} catch (IOException e) {
					System.out.println(e.getStackTrace());
				}

			} while (exitpointer != 1);

			double width2 = 0;
			String inputWidth2;
			do {
				try {
					System.out.println("Input width of envelope �2");
					BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
					inputWidth2 = br1.readLine();
					if (Double.parseDouble(inputWidth2.replace(',', '.')) > 0) {
						width2 = Double.parseDouble(inputWidth2.replace(',', '.'));
						exitpointer = 1;
					} else {
						System.out.println("Error! Input value must positive");
						exitpointer = 2;
					}
				} catch (NullPointerException e) {
					System.out.println("Error! Input value must not be a null.");
					exitpointer = 2;
				} catch (NumberFormatException e) {
					System.out.println("Error! Input value must must be double.");
					exitpointer = 2;
				} catch (IOException e) {
					System.out.println(e.getStackTrace());
				}
			} while (exitpointer != 1);

			double heigth2 = 0;
			String inputHeigth2;

			do {
				try {
					System.out.println("Input heigth of envelope �2");
					BufferedReader br1 = new BufferedReader(new InputStreamReader(System.in));
					inputHeigth2 = br1.readLine();
					if (Double.parseDouble(inputHeigth2.replace(',', '.')) > 0) {
						heigth2 = Double.parseDouble(inputHeigth2.replace(',', '.'));
						exitpointer = 1;
					} else {
						System.out.println("Error! Input value must positive");
						exitpointer = 2;
					}
				} catch (NullPointerException e) {
					System.out.println("Error! Input value must not be a null.");
					exitpointer = 2;
				} catch (NumberFormatException e) {
					System.out.println("Error! Input value must must be double.");
					exitpointer = 2;
				} catch (IOException e) {
					System.out.println(e.getStackTrace());
				}

			} while (exitpointer != 1);

			Envelope env1 = new Envelope();
			Envelope env2 = new Envelope();

			env1.setWidth(width1);
			env1.setHeigth(heigth1);
			env2.setWidth(width2);
			env2.setHeigth(heigth2);
			System.out.println(env1.putInside(env2));
			String yesnochoice;
			System.out.println("...to continue press \"Yes\" or \"Y");

			try {
				BufferedReader br5 = new BufferedReader(new InputStreamReader(System.in));
				yesnochoice = br5.readLine();
				yesnochoice = yesnochoice.toLowerCase();
				if (yesnochoice.equals("yes") || yesnochoice.equals("y")) {
					exitpointer = 1;
				} else {
					System.out.println("Goodbay!)");
					System.exit(0);
				}
			} catch (IOException e) {
				System.out.println(e.getStackTrace());
			}
		} while (exitpointer == 1);

	}
}
